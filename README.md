# README #

Node API Server
### What is this repository for? ###

* Quick summary
This is express API server.
* Version 1.0

### How do I get set up? ###
* Summary of set up

  1. Clone repo 

     git clone https://{yourUserName}@bitbucket.org/ashutec/node-api.git

  2. Install dependency

     npm install 

* Configuration

  1. Verify configuration in environment specific configuration file 

  2. When CORS is enabled ensure correct domains are white listed 

### Contribution guidelines ###
* Code review

before pushing code ensure it is ESlinted and does not any error

* Other guidelines

install ESlint extension in Visual studio code

### Who do I talk to? ###
* Repo owner or admin
